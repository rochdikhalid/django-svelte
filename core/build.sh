#!/usr/bin/env bash

# Exit on error
set -o errexit

# Install dependencies
pip install -r requirements.txt

# Serve static files
#python manage.py collectstatic --no-input

# Make and apply migrations
cd core && python manage.py makemigrations 
python manage.py migrate

# Create intial superuser
#./scripts/create_initial_contributor.sh

# Create initial data
#./scripts/create_initial_data.sh

