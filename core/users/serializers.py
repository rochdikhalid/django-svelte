from rest_framework import serializers

from .models import Contributor



class ContributorSerializer(serializers.ModelSerializer):
    """
    - checks user credentials are valid
    - translates and saves user data into JSOM format
    """

    class Meta:
        model = Contributor
        fields = ['id', 'email', 'username', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = Contributor(
            email = validated_data['email'],
            username = validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user

