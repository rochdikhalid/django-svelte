from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import Contributor, Profile



@receiver(post_save, sender = Contributor)
def create_profile(sender, instance, created, **kwargs):
    """creates a profile once the user is successufully created"""

    if created:
        Profile.objects.create(contributor = instance)


@receiver(post_save, sender = Contributor)
def save_profile(sender, instance, **kwargs):
    """saves the profile of the newly created user"""

    instance.profile.save()
