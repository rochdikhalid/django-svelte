from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin


class ContributorManager(BaseUserManager):

    def create_user(self, email, username, password = None):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email = self.normalize_email(email),
            username = username
        )
        user.set_password(password)
        user.save(using = self._db)
        return user

    def create_superuser(self, email, username, password = None):
        user = self.create_user(
            email,
            password = password,
            username = username,
        )
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using = self._db)
        return user
                  
class Contributor(AbstractBaseUser, PermissionsMixin):

    email = models.EmailField(max_length = 255, unique = True)
    username = models.CharField(max_length = 150, unique = True)
    is_active = models.BooleanField(default = True)
    is_staff = models.BooleanField(default = False)
    is_superuser = models.BooleanField(default = False)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    objects = ContributorManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.username

    class Meta: 
        verbose_name_plural = 'users'


class Profile(models.Model):

    contributor = models.OneToOneField(Contributor, on_delete = models.CASCADE)
    image = models.ImageField(default = 'standard.jpg', upload_to = 'profile_pics')

    def __str__(self):
        return f"{self.contributor.username}'s profile"
