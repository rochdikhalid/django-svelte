from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import Group
from .models import Contributor, Profile



class UserAdminConfig(BaseUserAdmin):

    model = Contributor

    list_display = [
        'id', 
        'email',
        'username',
        'is_active',
        'is_staff',
        'is_superuser'
    ]

    list_filter = ['is_active',]

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('username',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2'),
        }),
    )
    
    ordering = ('id',)

admin.site.register(Contributor, UserAdminConfig)
admin.site.register(Profile)
admin.site.unregister(Group)
