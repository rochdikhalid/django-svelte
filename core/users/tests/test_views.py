#from django.test import SimpleTestCase 
from django.test import TestCase
from django.urls import reverse

from users.models import Contributor


"""
TestCase is going to create a temporary clean database
which will be detroyed after the unit test stops
"""

class TestConfig(TestCase):

    @classmethod
    def setUpTestData(cls):
        Contributor.objects.create_user(
            email = 'randomuser@example.com',
            username = 'randomuser',
            password = '2HJ1vRV0Z&3iP'
        )

    def setUp(self):
        self.user_data = {
            'email': 'randomuser1@example.com',
            'username': 'randomuser1',
            'password': '2HJ1vRV0Z&3iA'
        }
""
class TestRegisterView(TestConfig):

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/auth/register/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('register_user'))
        self.assertEqual(response.status_code, 200)

    def test_user_cannot_register_without_data(self):
        response = self.client.post('/auth/register/')
        self.assertEqual(response.status_code, 400)

    def test_user_can_register_successfully(self):
        response = self.client.post(
            '/auth/register/',
            self.user_data,
            format = 'json',
        )
        self.assertEqual(response.status_code, 201)        

    def test_user_cannot_register_if_email_existed(self):
        self.user_data['email'] = 'randomuser@gmail.com'
        response = self.client.post(
            '/auth/register/',
            self.user_data,
            format = 'json',
        )
        self.assertEqual(response.status_code, 400)

    def test_user_cannot_register_if_username_existed(self):
        self.user_data['username'] = 'randomuser'
        response = self.client.post(
            '/auth/register/',
            self.user_data,
            format = 'json',
            )
        self.assertEqual(response.status_code, 400)