from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import Contributor, Profile



class ContributorCreationForm(UserCreationForm):
	""" A form to create a new contributor """

	class Meta:
		model = Contributor
		fields = ('username', 'email')


class ContributorChangeForm(UserChangeForm):
	""" A form to change contributor's information and permission in the admin """

	class Meta:
		model = Contributor
		fields = ('username', 'email')



class ContributorUpdateForm(forms.ModelForm):
	""" A form to update the contributor model """

	class Meta:
		model = Contributor
		fields = ('username', 'email')


class ProfileUpdateForm(forms.ModelForm):

	class Meta:
		model = Profile 
		fields = ('image',)
