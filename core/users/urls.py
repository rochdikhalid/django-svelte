from django.urls import path
from .views import ContributorList



app_name = 'users'


urlpatterns = [
    path('', ContributorList.as_view(), name = 'contributors-list'),
]